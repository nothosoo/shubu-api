<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('create', 'ProductsController@store');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

    //Product
    Route::prefix('admin')->group(function () {
        Route::post('product', 'ProductsController@store');
        Route::put('product/{id}', 'ProductsController@update');
        Route::delete('product/{id}', 'ProductsController@destroy');
        Route::get('products', 'ProductsController@index');
        Route::get('product/{id}', 'ProductsController@show');

        Route::get('orders', 'OrdersController@index');
        // Route::get('order/approve', 'OrdersController@approve');
    });

});

//Route::group(['middleware' => 'guest:api'], function () {
    Route::post('test', 'ProductsController@test');
    //Guest shows product
    Route::get('products', 'ProductsController@index')->name('product.index');
    Route::get('product/{id}', 'ProductsController@show')->name('product.show');

    //Return available images
    Route::get('product/order/images', 'ProductsController@orderImage')->name('order.image');

    Route::post('order', 'OrdersController@store')->name('order.store');

    Route::post('login', 'Auth\LoginController@login');
    //Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('oauth/{provider}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{provider}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
//});
