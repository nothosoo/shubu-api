<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImage;
use App\Http\Resources\ProductResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderImageResource;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $products = Product::where('status', 1)->paginate(12);
        return ProductResource::collection($products);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $product = Product::find($id);
        return new ProductResource($product);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'gender' => 'required',
            'material' => 'required',
            'color' => 'required',
            'count' => 'required|numeric',
            'product_type' => 'required',
            'price' => 'required|numeric',
            'images[]' => 'nullable',
            'deleteHashes[]' => 'nullable'
        ]);
        
        $product = new Product();
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->gender = $request->input('gender');
        $product->material = $request->input('material');
        $product->color = $request->input('color');
        $product->product_type = $request->input('product_type');
        $product->price = $request->input('price');
        $product->count = $request->input('count');
        $product->sale_percentage = $request->input('sale_percentage');
        $product->user_id = 1;
        $product->save();

        if(!empty($request->input('images')) && !empty($request->input('deleteHashes'))){
            $hashes = $request->input('deleteHashes');
            foreach($request->input('images') as $key => $img){
                
                $imga = new ProductImage();
                $imga->image_url = $img;
                $imga->delete_hash = $hashes[$key];
                $imga->product_id = $product->id;
                $imga->save();
            }
            
        }
        
        return new ProductResource($product);

    }

    public function test(Request $request){
        if($request->hasFile('images')){
            return $request;
        }
        return false;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //return $request;
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'gender' => 'required',
            'material' => 'required',
            'color' => 'required',
            'count' => 'required|numeric',
            'product_type' => 'required',
            'price' => 'required|numeric',
            'sale' => 'numeric',
            'images' => 'nullable'
        ]);

        $product = Product::find($id);

        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->product_type = $request->input('product_type');
        $product->material = $request->input('material');
        $product->color = $request->input('color');
        $product->gender = $request->input('gender');
        $product->count = $request->input('count');
        $product->price = $request->input('price');
        $product->sale_percentage = $request->input('sale_percentage');
        $product->save();

        return new ProductResource($product);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $product = Product::find($id)->delete();
        return response()->json(['success' => true]);
    }

    public function orderImage(){

        // foreach(glob('../client/static/img/order/print/*.*') as $filename){
        //     $images[] = $filename;
        // }
        
        $images = scandir("../client/static/img/order/print", 1);

        return new OrderImageResource(collect($images));
    }
}
