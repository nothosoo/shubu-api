<?php

use Illuminate\Database\Seeder;
use App\ProductImage;

class ProductImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/SgDBW7J.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 1;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/gfXlpai.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 1;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/jSb5vg6.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 1;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/7eH8nMV.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 1;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/SgDBW7J.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 2;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/gfXlpai.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 2;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/jSb5vg6.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 2;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/7eH8nMV.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 2;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/SgDBW7J.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 3;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/gfXlpai.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 3;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/jSb5vg6.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 3;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/7eH8nMV.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 3;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/SgDBW7J.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 4;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/gfXlpai.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 4;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/jSb5vg6.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 4;
        $img->save();

        $img = new ProductImage();
        $img->image_url = 'https://i.imgur.com/7eH8nMV.jpg';
        $img->delete_hash = 'null';
        $img->product_id = 4;
        $img->save();

    }
}
