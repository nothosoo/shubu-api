<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'Hosoo';
        $admin->email = 'hosoo.xoc@gmail.com';
        $admin->password = Hash::make('saitama');
        $admin->save();
    }
}
