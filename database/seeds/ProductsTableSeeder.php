<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        
        $product = new Product();
        $product->name = 'Цүнхний нэр 1';
        $product->user_id = 1;
        $product->description = "Аль ч насны хүн барих боломжтой даавуун цүнх";
        $product->price = 25000;
        $product->product_type = "Даавуун цүнх";
        $product->material = "Даавуун";
        $product->color = "Шар";
        $product->gender = "Эмэгтэй";
        $product->save();

        $product = new Product();
        $product->name = 'Цүнхний нэр 2';
        $product->user_id = 1;
        $product->description = "Аль ч насны хүн барих боломжтой даавуун цүнх";
        $product->price = 25000;
        $product->product_type = "Даавуун цүнх";
        $product->material = "Даавуун";
        $product->color = "Шар";
        $product->gender = "Эмэгтэй";
        $product->save();

        $product = new Product();
        $product->name = 'Цүнхний нэр 3';
        $product->user_id = 1;
        $product->description = "Аль ч насны хүн барих боломжтой даавуун цүнх";
        $product->price = 25000;
        $product->product_type = "Даавуун цүнх";
        $product->material = "Даавуун";
        $product->color = "Шар";
        $product->gender = "Эмэгтэй";
        $product->save();

        $product = new Product();
        $product->name = 'Цүнхний нэр 4';
        $product->user_id = 1;
        $product->description = "Аль ч насны хүн барих боломжтой даавуун цүнх";
        $product->price = 25000;
        $product->product_type = "Даавуун цүнх";
        $product->material = "Даавуун";
        $product->color = "Шар";
        $product->gender = "Эмэгтэй";
        $product->save();
    }
}
